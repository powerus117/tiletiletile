﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DataController : MonoBehaviour {

    public Color[] possibleColors;

    private PlayerData playerData;

	void Start ()
    {
        DontDestroyOnLoad(gameObject);

        LoadPlayerProgress();

        SceneManager.LoadScene("StartMenu");
    }

    public void SubmitNewPlayerScore(int newScore)
    {
        if (newScore > playerData.highScore)
        {
            playerData.highScore = newScore;
            SavePlayerScore();
        }
    }

    public void AddCurrency(int amount)
    {
        playerData.currency += amount;
        SavePlayerCurrency();
    }

    public void SetTopLayerColor(int index)
    {
        playerData.topLayerColIndex = index;
        SaveColors();
    }

    public void SetMiddleLayerColor(int index)
    {
        playerData.middleLayerColIndex = index;
        SaveColors();
    }

    public void SetBottomLayerColor(int index)
    {
        playerData.bottomLayerColIndex = index;
        SaveColors();
    }

    public int GetHighestPlayerScore()
    {
        return playerData.highScore;
    }

    public int GetCurrentCurrency()
    {
        return playerData.currency;
    }

    public Color GetTopLayerCol()
    {
        return possibleColors[playerData.topLayerColIndex];
    }

    public Color GetMiddleLayerCol()
    {
        return possibleColors[playerData.middleLayerColIndex];
    }

    public Color GetBottomLayerCol()
    {
        return possibleColors[playerData.bottomLayerColIndex];
    }

    private void LoadPlayerProgress()
    {
        playerData = new PlayerData();

        if (PlayerPrefs.HasKey("highScore"))
        {
            playerData.highScore = PlayerPrefs.GetInt("highScore");
        }

        if (PlayerPrefs.HasKey("currency"))
        {
            playerData.currency = PlayerPrefs.GetInt("currency");
        }

        if(PlayerPrefs.HasKey("topLayerColIndex"))
        {
            playerData.topLayerColIndex = PlayerPrefs.GetInt("topLayerColIndex");
        }

        if (PlayerPrefs.HasKey("middleLayerColIndex"))
        {
            playerData.middleLayerColIndex = PlayerPrefs.GetInt("middleLayerColIndex");
        }

        if (PlayerPrefs.HasKey("bottomLayerColIndex"))
        {
            playerData.bottomLayerColIndex = PlayerPrefs.GetInt("bottomLayerColIndex");
        }
    }

    private void SavePlayerScore()
    {
        PlayerPrefs.SetInt("highScore", playerData.highScore);
    }

    private void SavePlayerCurrency()
    {
        PlayerPrefs.SetInt("currency", playerData.currency);
    }

    private void SaveColors()
    {
        PlayerPrefs.SetInt("topLayerColIndex", playerData.topLayerColIndex);
        PlayerPrefs.SetInt("middleLayerColIndex", playerData.middleLayerColIndex);
        PlayerPrefs.SetInt("bottomLayerColIndex", playerData.bottomLayerColIndex);
    }
}
