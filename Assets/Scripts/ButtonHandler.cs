﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using UnityEngine.SceneManagement;

public class ButtonHandler : MonoBehaviour {
    public float timeLeft;
    public bool isButtonActive;
    public Color highlightColor = Color.red;

    public GameObject clickParticles;
    public float particleSurviveTime = 0.5f;

    public float fillerAlpha = 0.3f;

    private Color startCol;
    private GameObject currentCube;
    private Tweener punchTween;

    [HideInInspector]
    public Image tileFiller;
    private GameplayManager gameplayManager;

    private void Awake()
    {
        startCol = GetComponent<Image>().color;
        GetComponent<Image>().DOFade(0f, 0f);
        punchTween = transform.DOPunchScale(transform.lossyScale, 0.5f).SetAutoKill(false).Pause();
        GetComponent<Image>().DOFade(1f, 2f);
        gameplayManager = FindObjectOfType<GameplayManager>();

        foreach (Transform child in transform)
        {
            if(child.gameObject.CompareTag("TileFiller"))
            {
                tileFiller = child.GetComponent<Image>();
            }
        }
    }

    public void TriggerPressEvent(float timeToPress, float delay, GameObject theCube)
    {
        StartCoroutine(StartPressEvent(timeToPress, delay, theCube));
    }

    public IEnumerator StartPressEvent(float timeToPress, float delay, GameObject theCube)
    {
        yield return new WaitForSeconds(delay);
        GetComponent<Image>().color = highlightColor;
        tileFiller.DOFade(fillerAlpha, 0.1f);
        timeLeft = timeToPress;
        isButtonActive = true;
        currentCube = theCube;
    }

    public void ButtonHit()
    {
        if(isButtonActive)
        {
            SetButtonInactive();
            gameplayManager.AddScore(1);

            GameObject newPs = Instantiate(clickParticles, transform.position, transform.rotation);
            Destroy(newPs, particleSurviveTime);

            if(currentCube != null)
            {
                Destroy(currentCube);
            }
        }
        else
        {
            SceneManager.LoadScene("GameOver");
        }

        punchTween.Restart();
    }

    void Update () {
		if(timeLeft > 0)
        {
            //Player needs to press button
            timeLeft -= Time.deltaTime;

            if(timeLeft < 0)
            {
                //End of button press
                SetButtonInactive();
                SceneManager.LoadScene("GameOver");
            }
        }
        //Button inactive
	}

    private void SetButtonInactive()
    {
        isButtonActive = false;
        GetComponent<Image>().color = startCol;
        tileFiller.DOFade(0f, 0.1f);
        timeLeft = 0;
    }

    public void SetButtonColor(Color col)
    {
        GetComponent<Image>().color = col;
        startCol = col;

        col.a = tileFiller.color.a;
        tileFiller.color = col;
    }
}
