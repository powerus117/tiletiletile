﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeMover : MonoBehaviour {

    private Transform target;
    private float timeToFly;
    private Vector3 startPos;
    private float startTime;

    private void Start()
    {
        startPos = transform.position;
        startTime = Time.time;
    }

    public void SetMoveTarget(Transform target, float timeToFly)
    {
        this.target = target;
        this.timeToFly = timeToFly;
    }

	void Update () {
        float timeElapsed = Time.time - startTime;
        float precentage = timeElapsed / timeToFly;
		transform.position = Vector3.Lerp(startPos, target.position, precentage);
        transform.rotation = Quaternion.Slerp(Quaternion.identity, target.rotation, precentage);
    }
}
