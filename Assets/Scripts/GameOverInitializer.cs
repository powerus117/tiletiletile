﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameOverInitializer : MonoBehaviour {
    public Text currentScoreText;
    public Text highScoreText;
    public Text currencyText;

    private DataController dataController;

	void Start ()
    {
        dataController = FindObjectOfType<DataController>();

        // Highscore
        dataController.SubmitNewPlayerScore(GameplayManager.gameScore);
        highScoreText.text = dataController.GetHighestPlayerScore().ToString();

        currentScoreText.text = GameplayManager.gameScore.ToString();

        //Currency
        dataController.AddCurrency(GameplayManager.gameScore);
        currencyText.text = dataController.GetCurrentCurrency().ToString();
	}

}
