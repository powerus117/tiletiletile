﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CurrencyTextSetter : MonoBehaviour {

    private DataController dataController;
    private Text currencyText;

	void Start () {
        dataController = FindObjectOfType<DataController>();
        currencyText = GetComponent<Text>();

        SetCurrencyText();
    }

    public void SetCurrencyText()
    {
        currencyText.text = dataController.GetCurrentCurrency().ToString();
    }
}
