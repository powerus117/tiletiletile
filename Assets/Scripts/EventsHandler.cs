﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class EventsHandler : MonoBehaviour {
    public GameObject buttonGroup;

    public MoveEventData moveEvent;
    private Vector3 startPos;

    public RotateEventData rotateEvent;

    private void Start()
    {
        startPos = buttonGroup.transform.position;
    }

    public void OnScoreChanged(int currentScore)
    {
        if (rotateEvent.nodesLeftToEnd > 0)
        {
            rotateEvent.nodesLeftToEnd--;

            if (rotateEvent.nodesLeftToEnd <= 0)
            {
                EndRotateGridEvent();
            }
        }

        if ((currentScore - moveEvent.scoreForFirstEvent) % moveEvent.scoreToRepeatEvent == 0 && currentScore >= moveEvent.scoreForFirstEvent)
        {
            StartMoveGridEvent();
        }

        if((currentScore - rotateEvent.scoreForFirstEvent) % rotateEvent.scoreToRepeatEvent == 0 && currentScore >= rotateEvent.scoreForFirstEvent)
        {
            StartRotateGridEvent();
        }
    }

    public void StartMoveGridEvent()
    {
        Debug.Log("Move Event Started");

        buttonGroup.transform.DOMove(moveEvent.moveToPos.position, moveEvent.timeToMove / 2).SetEase(Ease.Linear).OnComplete(() => buttonGroup.transform.DOMove(startPos, moveEvent.timeToMove / 2).SetEase(Ease.Linear));
    }

    public void StartRotateGridEvent()
    {
        Debug.Log("Rotate Event Started");

        rotateEvent.nodesLeftToEnd = rotateEvent.eventLength;
        buttonGroup.transform.DORotate(new Vector3(0, 0, rotateEvent.rotateAmountDeg), rotateEvent.timeToMove).SetEase(Ease.Linear);
    }

    private void EndRotateGridEvent()
    {
        Debug.Log("Rotate Event Ended");

        buttonGroup.transform.DORotate(Vector3.zero, rotateEvent.timeToMove).SetEase(Ease.Linear);
        rotateEvent.IncreaseRotateAmount();
    }
}

[System.Serializable]
public class EventData
{
    public int scoreForFirstEvent = 20;
    public int scoreToRepeatEvent = 20;
    public float timeToMove = 3f;
}

[System.Serializable]
public class MoveEventData : EventData
{
    public Transform moveToPos;
}

[System.Serializable]
public class RotateEventData : EventData
{
    public int eventLength = 10;
    public float rotateAmountDeg = 30f;
    public float rotateAmountDegIncrease = 30f;
    public float timeToMoveIncrease = 3f;

    [HideInInspector]
    public int nodesLeftToEnd = 0;

    public void IncreaseRotateAmount()
    {
        rotateAmountDeg += rotateAmountDegIncrease;
        timeToMove += timeToMoveIncrease;
    }
}