﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class GameplayManager : MonoBehaviour {

    public static int gameScore = 0;

    public ButtonHandler[] buttons;
    public Color topLayerCol, middleLayerCol, bottomLayerCol;
    public Text scoreText;
    public Transform spawnLocation;
    public GameObject cubePrefab;


    public float timeToNextNote;
    public float minimumTimeToNextNote = 0.6f;
    public float noteFlyTime = 3f;
    public float timeToPressNote = 0.7f;
    [HideInInspector]
    public float currentTimeToNextNote;

    private EventsHandler eventsHandler;
    [HideInInspector]
    public DataController dataController;

    private void Start()
    {
        currentTimeToNextNote = timeToNextNote;
        ResetScore();

        eventsHandler = FindObjectOfType<EventsHandler>();
        scoreText.text = gameScore.ToString();

        dataController = FindObjectOfType<DataController>();
        SetColors();
    }

    void Update () {
		if(currentTimeToNextNote > 0)
        {
            currentTimeToNextNote -= Time.deltaTime;
        }
        else
        {
            // Spawn note
            currentTimeToNextNote = timeToNextNote;

            SpawnNote(Random.Range(0, buttons.Length));
        }

        if(timeToNextNote > minimumTimeToNextNote)
        {
            timeToNextNote -= Time.deltaTime * 0.08f;
        }
    }

    private void SpawnNote(int noteIndex)
    {
        GameObject newCube = Instantiate(cubePrefab, spawnLocation.position, spawnLocation.rotation);

        //Moving cube
        newCube.GetComponent<CubeMover>().SetMoveTarget(buttons[noteIndex].transform, noteFlyTime);
        Destroy(newCube, noteFlyTime);

        //Coloring cube
        if(noteIndex < 3)
            newCube.GetComponent<MeshRenderer>().material.SetColor("_Color", topLayerCol);
        else if (noteIndex < 6)
            newCube.GetComponent<MeshRenderer>().material.SetColor("_Color", middleLayerCol);
        else
            newCube.GetComponent<MeshRenderer>().material.SetColor("_Color", bottomLayerCol);

        buttons[noteIndex].TriggerPressEvent(timeToPressNote, noteFlyTime - timeToPressNote * 0.8f, newCube);
    }

    public void AddScore(int score)
    {
        gameScore += score;
        scoreText.text = gameScore.ToString();

        OnScoreChanged(score);
        eventsHandler.OnScoreChanged(gameScore);
    }

    public void OnScoreChanged(int score)
    {

    }

    public void ResetScore()
    {
        gameScore = 0;
    }

    private void SetColors()
    {
        if (dataController == null)
            return;

        //Spawning block col
        topLayerCol = dataController.GetTopLayerCol();
        middleLayerCol = dataController.GetMiddleLayerCol();
        bottomLayerCol = dataController.GetBottomLayerCol();

        //Image colors
        for (int i = 0; i < 9; i++)
        {
            if(i < 3)
            {
                buttons[i].SetButtonColor(dataController.GetTopLayerCol());
            }
            else if(i < 6)
            {
                buttons[i].SetButtonColor(dataController.GetMiddleLayerCol());
            }
            else if(i < 9)
            {
                buttons[i].SetButtonColor(dataController.GetBottomLayerCol());
            }
        }
    }
}
