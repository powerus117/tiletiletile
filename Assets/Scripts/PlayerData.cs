﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerData {

    public int highScore = 0;
    public int currency = 0;
    public int topLayerColIndex = 6, middleLayerColIndex = 1, bottomLayerColIndex = 2;
}
