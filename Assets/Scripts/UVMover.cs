﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UVMover : MonoBehaviour {
    public float scrollSpeed = -0.5F;
    private Material mat;

	void Start () {
        mat = GetComponent<MeshRenderer>().material;
	}

	void Update () {
        float offset = Time.time * scrollSpeed;
        mat.SetTextureOffset("_MainTex", new Vector2(0, offset));
    }
}
